package com.tcbakes.untestable;

import com.mashape.unirest.http.JsonNode;
import com.tcbakes.untestable.command.Command;
import com.tcbakes.untestable.io.*;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * A suite of tests that validate the correct behavior of
 * {@link BitbucketClientApp}
 */
public class BitbucketClientAppTest {

    @InjectMocks
    BitbucketClientApp app;

    @Mock
    UserIO mockIo;

    Command mockCommand;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        List<Command> mockCommands = new ArrayList<>();
        mockCommand = mock(Command.class);
        mockCommands.add(mockCommand);
        app.setCommands(mockCommands);

    }

    /**
     * A handle-able command should be delegated to that command.
     */
    @Test
    public void test1() throws Exception {

        //When a command can handle the user's command request
        when(mockCommand.canHandle(anyString())).thenReturn(true);

        //And the app is run
        app.run(new String[]{"any-command"});

        //The app should delegate the running of the command to the command instance
        verify(mockCommand).run();
    }

    /**
     * An unrecognized command should display an error to the user.
     */
    @Test
    public void test2() throws Exception {

        //When no command exists capable of handling the user's command request
        when(mockCommand.canHandle(anyString())).thenReturn(false);

        //And the app is run
        app.run(new String[]{"any-command"});

        //The app should not call the command
        verify(mockCommand, never()).run();
        //The app should display a message back to the user explaining that the provided command is not supported
        verify(mockIo).display(eq("Unrecognized command 'any-command'"));
    }

    /**
     * Print a message if no command is provided
     */
    @Test
    public void test3() throws Exception {

        //When there is at least one command
        when(mockCommand.getName()).thenReturn("some-command");

        //And no command argument is given
        app.run(new String[]{});

        //The app should display a message back to the user, listing the available commands
        verify(mockIo).display(eq("No command provided. Nothing to do.\n" +
                "Supported commands are:\n" +
                "'some-command'\n"));
    }

}
