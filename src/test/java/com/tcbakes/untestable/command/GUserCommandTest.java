package com.tcbakes.untestable.command;

import com.mashape.unirest.http.JsonNode;
import com.tcbakes.untestable.flow.Flow;
import com.tcbakes.untestable.io.*;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test the {@link GUserCommand}
 */
public class GUserCommandTest {

    @InjectMocks
    GUserCommand command;

    @Mock
    UserIO mockIo;

    @Mock
    BitbucketClient mockBb;

    @Mock
    Flow<Credentials> mockFl;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    /**
     * The command should appropriately hand errors from the service and display
     * a message
     */
    @Test
    public void test1() throws Exception {

        //When there are credentials
        when(mockFl.run()).thenReturn(new Credentials("foo", "bar"));
        //And when the bitbucket getUser service responds with and error
        when(mockBb.getUser(any(Credentials.class)))
                .thenReturn(new BitbucketResponse(HttpStatus.SC_UNAUTHORIZED, "Unauthorized", null));

        //And the command is run
        command.run();

        //The command should display the a simple explanation of the cause of the error
        verify(mockIo).display(eq("Problem calling the service. Response code: 401"));

    }

    /**
     * The command should appropriately display the user profile when it is successfully returned from the
     * service.  It should remove the "repositories" property from the payload prior to displayin it.
     */
    @Test
    public void test2() throws Exception {

        //When there are credentials
        when(mockFl.run()).thenReturn(new Credentials("foo", "bar"));
        //And when the bitbucket getUser service responds with user profile information, not nicely formatted
        // (note that in this payload, indentation has been removed)
        when(mockBb.getUser(any(Credentials.class)))
                .thenReturn(new BitbucketResponse(HttpStatus.SC_OK, "OK", new JsonNode(
                        "{\n" +
                                "\"repositories\": [],\n" +
                                "\"user\": {\n" +
                                "\"username\": \"tcbakes\",\n" +
                                "\"first_name\": \"Tristan\",\n" +
                                "\"last_name\": \"Baker\",\n" +
                                "\"display_name\": \"Tristan Baker\",\n" +
                                "\"is_staff\": false,\n" +
                                "\"avatar\": \"https://bitbucket.org/account/tcbakes/avatar/32/?ts=0\",\n" +
                                "\"resource_uri\": \"/1.0/users/tcbakes\",\n" +
                                "\"is_team\": false\n" +
                                "}\n" +
                                "}")));

        //And the command is run
        command.run();

        //The command should display the user profile information, formatted nicely, with the "repositories" property removed
        verify(mockIo).display(eq("{\"user\": {\n" +
                "    \"is_staff\": false,\n" +
                "    \"resource_uri\": \"/1.0/users/tcbakes\",\n" +
                "    \"last_name\": \"Baker\",\n" +
                "    \"avatar\": \"https://bitbucket.org/account/tcbakes/avatar/32/?ts=0\",\n" +
                "    \"display_name\": \"Tristan Baker\",\n" +
                "    \"first_name\": \"Tristan\",\n" +
                "    \"is_team\": false,\n" +
                "    \"username\": \"tcbakes\"\n" +
                "}}"));
    }

}
