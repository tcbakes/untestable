package com.tcbakes.untestable.command;

import com.mashape.unirest.http.JsonNode;
import com.tcbakes.untestable.flow.Flow;
import com.tcbakes.untestable.io.*;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test the {@link GUserReposCommand}
 */
public class GUserReposCommandTest {

    @InjectMocks
    GUserReposCommand command;

    @Mock
    Flow<Credentials> mockFl;

    @Mock
    UserIO mockIo;

    @Mock
    BitbucketClient mockBb;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }
    /**
     * Test that when 'g-user-repos' is given a bad password, an error is
     * appropriately written to the output
     */
    @Test
    public void test1() throws Exception {

        //When there are credentials
        when(mockFl.run()).thenReturn(new Credentials("foo", "bar"));
        //And when the bitbucket getUserRepos service responds with "Unauthorized"
        when(mockBb.getUserRepos(any(Credentials.class)))
                .thenReturn(new BitbucketResponse(HttpStatus.SC_UNAUTHORIZED, "Unauthorized", null));

        //And the command is run
        command.run();

        //The command should display the "unauthorized" message back to the user
        verify(mockIo).display(eq("Problem calling the service. Response code: 401"));

    }



    /**
     * Test that when 'g-user-repos' is called with valid credentials, the bitbucket user profile service is called
     * and the results are displayed back to the user
     */
    @Test
    public void test2() throws Exception {

        //When there are credentials
        when(mockFl.run()).thenReturn(new Credentials("foo", "bar"));
        //And when the bitbucket getUserProfile service responds with the user's repositories, not nicely formatted
        // (note that in this payload, indentation has been removed)
        when(mockBb.getUserRepos(any(Credentials.class)))
                .thenReturn(new BitbucketResponse(HttpStatus.SC_OK, "OK", new JsonNode(
                        "[{\n" +
                                "\"is_private\": false,\n" +
                                "\"no_public_forks\": false,\n" +
                                "\"description\": \"Illustrates the evolution of an application from untestable to testable.\",\n" +
                                "\"language\": \"java\",\n" +
                                "\"has_wiki\": false,\n" +
                                "\"read_only\": false,\n" +
                                "\"logo\": \"https://bitbucket.org/tcbakes/untestable/avatar/32/?ts=1445056854\",\n" +
                                "\"state\": \"available\",\n" +
                                "\"scm\": \"git\",\n" +
                                "\"slug\": \"untestable\",\n" +
                                "\"owner\": \"tcbakes\",\n" +
                                "\"utc_created_on\": \"2015-10-15 04:45:18+00:00\",\n" +
                                "\"is_fork\": false,\n" +
                                "\"last_updated\": \"2015-10-17T06:40:54.696\",\n" +
                                "\"website\": \"\",\n" +
                                "\"creator\": null,\n" +
                                "\"is_mq\": false,\n" +
                                "\"mq_of\": null,\n" +
                                "\"utc_last_updated\": \"2015-10-17 04:40:54+00:00\",\n" +
                                "\"resource_uri\": \"/1.0/repositories/tcbakes/untestable\",\n" +
                                "\"fork_of\": null,\n" +
                                "\"has_issues\": false,\n" +
                                "\"no_forks\": false,\n" +
                                "\"email_mailinglist\": \"\",\n" +
                                "\"size\": 188561,\n" +
                                "\"created_on\": \"2015-10-15T06:45:18.942\",\n" +
                                "\"name\": \"untestable\"\n" +
                                "}]")));

        //And the command is run with the 'g-user-repos' command
        command.run();

        //The command should display the user's repositories information, nicely formatted
        verify(mockIo).display(eq(
                "[{\n" +
                        "    \"is_private\": false,\n" +
                        "    \"no_public_forks\": false,\n" +
                        "    \"description\": \"Illustrates the evolution of an application from untestable to testable.\",\n" +
                        "    \"language\": \"java\",\n" +
                        "    \"has_wiki\": false,\n" +
                        "    \"read_only\": false,\n" +
                        "    \"logo\": \"https://bitbucket.org/tcbakes/untestable/avatar/32/?ts=1445056854\",\n" +
                        "    \"state\": \"available\",\n" +
                        "    \"scm\": \"git\",\n" +
                        "    \"slug\": \"untestable\",\n" +
                        "    \"owner\": \"tcbakes\",\n" +
                        "    \"utc_created_on\": \"2015-10-15 04:45:18+00:00\",\n" +
                        "    \"is_fork\": false,\n" +
                        "    \"last_updated\": \"2015-10-17T06:40:54.696\",\n" +
                        "    \"website\": \"\",\n" +
                        "    \"creator\": null,\n" +
                        "    \"is_mq\": false,\n" +
                        "    \"mq_of\": null,\n" +
                        "    \"utc_last_updated\": \"2015-10-17 04:40:54+00:00\",\n" +
                        "    \"resource_uri\": \"/1.0/repositories/tcbakes/untestable\",\n" +
                        "    \"fork_of\": null,\n" +
                        "    \"has_issues\": false,\n" +
                        "    \"no_forks\": false,\n" +
                        "    \"email_mailinglist\": \"\",\n" +
                        "    \"size\": 188561,\n" +
                        "    \"created_on\": \"2015-10-15T06:45:18.942\",\n" +
                        "    \"name\": \"untestable\"\n" +
                        "}]"));
    }

}
