package com.tcbakes.untestable;

import com.tcbakes.untestable.spring.ConsoleConfig;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Unit test for simple Untestable.
 */
public class UntestableTest
{

    /**
     * The default configuration should be ConsoleConfig.class
     */
    @Test
    public void test1() {
        assertEquals(ConsoleConfig.class, Untestable.getConfig());
    }

    /**
     * The default should be passed as-is to the underlying app
     */
    @Test
    public void test2() throws Exception {

        ArgDrivenApp mockApp = mock(ArgDrivenApp.class);
        TestConfig.app = mockApp;

        Untestable.config = TestConfig.class;


        //When the main method is provided arguments.
        Untestable.main(new String[]{"whatever"});

        ArgumentCaptor<String[]> argsCaptor = ArgumentCaptor.forClass(String[].class);
        verify(mockApp).run(argsCaptor.capture());

        //The arguments should be passed as is to the underlying app
        assertEquals(1, argsCaptor.getValue().length);
        assertEquals("whatever", argsCaptor.getValue()[0]);
    }

    /**
     * Exceptions from the underlying app should be rethrown as-is
     */
    @Test(expected = Exception.class)
    public void test3() throws Exception {

        ArgDrivenApp mockApp = mock(ArgDrivenApp.class);
        TestConfig.app = mockApp;

        doThrow(new Exception("something went wrong")).when(mockApp).run(any(String[].class));

        Untestable.config = TestConfig.class;
        Untestable.main(new String[]{"whatever"});
    }


    /**
     * A spring configuration that configures the ArgDrivenApp
     */
    @ComponentScan({"com.tcbakes.untestable"})
    @Configuration
    static class TestConfig{
        static ArgDrivenApp app;

        @Bean
        ArgDrivenApp getApp(){
            return app;
        }
    }
}
