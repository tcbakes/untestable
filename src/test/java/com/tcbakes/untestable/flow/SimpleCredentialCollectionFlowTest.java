package com.tcbakes.untestable.flow;

import com.tcbakes.untestable.io.CredentialPersistence;
import com.tcbakes.untestable.io.Credentials;
import com.tcbakes.untestable.io.UserIO;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * A suite of tests for {@link SimpleCredentialCollectionFlowTest}
 */
public class SimpleCredentialCollectionFlowTest {

    @InjectMocks
    SimpleCredentialCollectionFlow flow;

    @Mock
    UserIO mockIo;

    @Mock
    CredentialPersistence mockCp;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    /**
     * When there are no stored credentials, the flow should collect them from the user
     */
    @Test
    public void test1() throws Exception {

        //When there are no stored credentials
        when(mockCp.retrieve()).thenReturn(null);
        //And when a username is provided
        when(mockIo.collect("Username: ")).thenReturn("tcbakes");
        //And when a password is provided
        when(mockIo.collectSensitive("Password: ")).thenReturn("fake-o");
        //And when the user doesn't want to have the credentials remembered
        when(mockIo.collect("Remember [y/n]: ")).thenReturn("n");

        //And the flow is run
        Credentials creds = flow.run();

        //The command should collect the username
        verify(mockIo).collect(eq("Username: "));
        //The command should collect the password as a piece of sensitive information
        verify(mockIo).collectSensitive(eq("Password: "));
        //The command should ask if the user wants their credentials remembered
        verify(mockIo).collect(eq("Remember [y/n]: "));
        //The command should not persist the credentials
        verify(mockCp, never()).persist(any(Credentials.class));

        //And the flow should return the collected credentials
        assertEquals("tcbakes", creds.username);
        assertEquals("fake-o", creds.password);

    }

    /**
     * When there are stored credentials, the flow should not collect them from the user
     * but simply collect them from the persisted location
     */
    @Test
    public void test2() throws Exception {

        //When there are saved and valid credentials
        when(mockCp.retrieve()).thenReturn(new Credentials("tcbakes", "fake-o"));

        //And the flow is run
        Credentials creds = flow.run();

        //The command should not collect the username
        verify(mockIo, never()).collect(eq("Username: "));
        //The command should not collect the password
        verify(mockIo, never()).collectSensitive(eq("Password: "));
        //The command should not ask if the user wants their credentials remembered
        verify(mockIo, never()).collect(eq("Remember [y/n]: "));

        //And the flow should return the persisted credentials
        assertEquals("tcbakes", creds.username);
        assertEquals("fake-o", creds.password);
    }

    /**
     *  When the user elects to save the credentials, they should be saved
     */
    @Test
    public void test3() throws Exception {
        //When there are no stored credentials
        when(mockCp.retrieve()).thenReturn(null);
        //And when the username is valid
        when(mockIo.collect("Username: ")).thenReturn("tcbakes");
        //And when the password is valid
        when(mockIo.collectSensitive("Password: ")).thenReturn("fake-o");
        //And when the user wants to have the credentials remembered
        when(mockIo.collect("Remember [y/n]: ")).thenReturn("y");

        //And the flow is run
        Credentials creds = flow.run();

        //The command should collect the username
        verify(mockIo).collect(eq("Username: "));
        //The command should collect the password as a piece of sensitive information
        verify(mockIo).collectSensitive(eq("Password: "));
        //The command should ask if the user wants their credentials remembered
        verify(mockIo).collect(eq("Remember [y/n]: "));
        //The command should persist the credentials
        verify(mockCp).persist(any(Credentials.class));

        //And the flow should return the collected and persisted credentials
        assertEquals("tcbakes", creds.username);
        assertEquals("fake-o", creds.password);
    }
}
