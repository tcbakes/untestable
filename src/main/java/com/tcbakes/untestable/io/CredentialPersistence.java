package com.tcbakes.untestable.io;

/**
 * Persistent access to a single set of credentials
 */
public interface CredentialPersistence {

    /**
     * Persist the credentials to the underlying storage. Previously persisted credentials will be overridden
     * @param creds the credentials to persiste.
     * @throws Exception if there is a problem accessing the underlying storage
     */
    void persist(Credentials creds) throws Exception;

    /**
     * Retrieve the credentials from the underlying storage
     * @return A set of credentials if found. null otherwise
     * @throws Exception if there is a problem accessing the underlying storage
     */
    Credentials retrieve() throws Exception;
}
