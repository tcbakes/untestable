package com.tcbakes.untestable.io;

import com.mashape.unirest.http.JsonNode;

/**
 * Access to a set of remote Bitbucket services for reading and manipulationg Bitbucket objects
 */
public interface BitbucketClient {

    /**
     * Get a user profile
     * <p>
     * See bitbucket's <a href="https://confluence.atlassian.com/bitbucket/user-endpoint-296092264
     * .html#userEndpoint-GETauserprofile">official documentation</a>
     *
     * @param creds identity and authorization to access this user's profile
     * @return The user's profile information
     */
    BitbucketResponse getUser(Credentials creds);

    /**
     * Get a user's set of repositories.
     * <p>
     * See bitbucket's <a href="https://confluence.atlassian.com/bitbucket/user-endpoint-296092264.html#userEndpoint-GETalistofrepositoriesvisibletoanaccount">official documentation</a>
     *
     * @param creds identity and authorization to access this user's repositories
     * @return The user's repositories
     */
    BitbucketResponse getUserRepos(Credentials creds);

}
