package com.tcbakes.untestable.io;

import static java.lang.System.*;

/**
 * IO to the {@link System#console()}
 */
public class ConsoleIO implements UserIO {

    @Override
    public String collect(String prompt, Object... args) {
        return console().readLine(prompt, args);
    }

    @Override
    public String collectSensitive(String prompt, Object... args) {
        return new String(console().readPassword(prompt, args));
    }

    @Override
    public void display(String info) {
        console().writer().println(info);
    }
}
