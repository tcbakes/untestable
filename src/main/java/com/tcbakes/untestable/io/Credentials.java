package com.tcbakes.untestable.io;

/**
 * A set of credentials for authenticating access to things.
 */
public class Credentials {
    /**
     * A username defining a unique identity
     */
    public String username;
    /**
     * A password authenticating an identity
     */
    public String password;

    public Credentials(String un, String pw){
        this.username = un;
        this.password = pw;
    }

}
