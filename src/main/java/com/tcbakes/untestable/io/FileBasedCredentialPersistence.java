package com.tcbakes.untestable.io;

import java.io.*;

/**
 * reads/writes the credentials to a file on the local file system
 */
public class FileBasedCredentialPersistence implements CredentialPersistence {

    File credsfile;

    public FileBasedCredentialPersistence(File f){
        this.credsfile = f;
    }

    /**
     * writes the credentials to the local file system, separating the username from the password
     * using a single <a href="https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html">line
     * .separator</a> character.
     * @param creds The credentials to persist
     * @throws Exception
     */
    @Override
    public void persist(Credentials creds) throws Exception{
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(credsfile,false))){
            bw.write(creds.username);
            bw.newLine();
            bw.write(creds.password);
        }
    }

    /**
     * reads the credentials from the local file system
     * @return The credentials if found, null otherwise
     * @throws Exception If there is an error accessing the file
     */
    @Override
    public Credentials retrieve() throws Exception{
        if(!this.credsfile.exists())
            return null;

        try (BufferedReader br = new BufferedReader(new FileReader(this.credsfile)))
        {
            return new Credentials(br.readLine(), br.readLine());
        }
    }
}
