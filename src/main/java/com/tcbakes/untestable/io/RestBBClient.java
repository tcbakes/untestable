package com.tcbakes.untestable.io;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Interacts with the Bitbucket REST services using the basic-auth authentication scheme
 */
public class RestBBClient implements BitbucketClient {

    @Override
    public BitbucketResponse getUser(Credentials creds) {

        HttpResponse<JsonNode> response;
        try {
            response = Unirest.get("https://api.bitbucket.org/1.0/user")
                    .basicAuth(creds.username, creds.password)
                    .asJson();

            return new BitbucketResponse(response.getStatus(), response.getStatusText(), response.getBody());

        } catch (UnirestException e) {
            throw new RuntimeException("Could not retrieve user profile information");
        }
    }

    @Override
    public BitbucketResponse getUserRepos(Credentials creds) {

        HttpResponse<JsonNode> response;
        try {
            response = Unirest.get("https://api.bitbucket.org/1.0/user/repositories")
                    .basicAuth(creds.username, creds.password)
                    .asJson();

            return new BitbucketResponse(response.getStatus(), response.getStatusText(), response.getBody());

        } catch (UnirestException e) {
            throw new RuntimeException("Could not retrieve user repository listing");
        }
    }
}
