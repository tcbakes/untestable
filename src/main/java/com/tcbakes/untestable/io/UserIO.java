package com.tcbakes.untestable.io;

/**
 * Prompt a user for information. Display information back to a user.
 */
public interface UserIO {

    /**
     * Prompt a user for information.
     * @param prompt A format string
     * @param args optional arguments for the format string
     * @return a value entered by the user in response to the prompt
     */
    String collect(String prompt, Object... args);

    /**
     * Similar to {@link #collect(String, Object...)}, except that the i
     * nformation being collected is sensitive, so care should be taken to appropriately
     * obscure/protect the information during display
     *
     * @param prompt A format string describing the information needed
     * @param args optional arguments for the format string
     * @return a value entered by the user in response to the prompt
     */
    String collectSensitive(String prompt, Object... args);

    /**
     * Display information to the user
     *
     * @param info The information to display
     */
    void display(String info);
}
