package com.tcbakes.untestable.io;

import com.mashape.unirest.http.JsonNode;
import org.apache.http.HttpStatus;

/**
 * Encapsulates the result of an Bitbucket Service request
 */
public class BitbucketResponse {

    /**
     * An HTTP status code describing the result of a request.
     */
    public int statusCode;

    /**
     * An en-US string describing the the statusCode
     */
    public String reason;

    /**
     * The service response
     */
    public JsonNode responseBody;

    public BitbucketResponse(int statusCode, String reason, JsonNode responseBody){
        this.statusCode = statusCode;
        this.reason = reason;
        this.responseBody = responseBody;
    }
}
