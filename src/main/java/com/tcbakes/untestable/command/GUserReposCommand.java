package com.tcbakes.untestable.command;

import com.tcbakes.untestable.flow.Flow;
import com.tcbakes.untestable.io.BitbucketClient;
import com.tcbakes.untestable.io.BitbucketResponse;
import com.tcbakes.untestable.io.Credentials;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Identified by the name "g-user-repos", gets access to the user's list of accessible bitbucket
 * repositories
 */
public class GUserReposCommand extends AbstractCommand {

    private static final String NAME = "g-user-repos";
    /**
     * A {@link BitbucketClient} instance for interacting with the remote Bitbucket services
     */
    private BitbucketClient bb;

    @Autowired
    public void setBb(BitbucketClient bb){
        this.bb = bb;
    }

    /**
     * A user flow that will collect credentials
     */
    private Flow<Credentials> cf;

    @Autowired
    public void setCf(Flow<Credentials> cf){
        this.cf = cf;
    }

    @Override
    public boolean canHandle(String command) {
        return NAME.equals(command);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void run() throws Exception {

        Credentials creds = cf.run();

        BitbucketResponse response = bb.getUserRepos(creds);
        if(response.statusCode == 200){
            org.json.JSONArray body = response.responseBody.getArray();
            io.display(body.toString(4));
        } else {
            io.display("Problem calling the service. Response code: " + response.statusCode);
        }
    }
}
