package com.tcbakes.untestable.command;

import com.tcbakes.untestable.flow.Flow;
import com.tcbakes.untestable.io.CredentialPersistence;
import com.tcbakes.untestable.io.Credentials;
import com.tcbakes.untestable.io.UserIO;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * All commands will interact with the user to collect/display information.  This
 * class provide access to an instance of an {@link UserIO} for doing just that.
 */
public abstract class AbstractCommand implements Command {
    /**
     * A {@link UserIO} instance for collecting info from and displaying info
     * to the user.
     */
    protected UserIO io;

    @Autowired
    public void setIo(UserIO io) {
        this.io = io;
    }

}
