package com.tcbakes.untestable.command;

/**
 * Something that interacts with a user to perform some useful activity
 * and is identifiable by a simple string command name.
 */
public interface Command {

    /**
     * Returns true if this can candle the command
     * @param command The name of the command
     * @return true if this can handle the request, false otherwise
     */
    boolean canHandle(String command);

    /**
     * The name of the command
     * @return the name
     */
    String getName();

    /**
     * Performs the command
     * @throws Exception an unrecoverable error occurs
     */
    void run() throws Exception;

}
