package com.tcbakes.untestable.command;

import com.tcbakes.untestable.flow.Flow;
import com.tcbakes.untestable.io.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Identified by the name 'g-user'.  Gets the user's bitbucket profile
 */
public class GUserCommand extends AbstractCommand {

    private static final String NAME = "g-user";
    /**
     * A {@link BitbucketClient} instance for interacting with the remote Bitbucket services
     */
    private BitbucketClient bb;

    @Autowired
    public void setBb(BitbucketClient bb){
        this.bb = bb;
    }

    /**
     * A user flow that will collect credentials
     */
    private Flow<Credentials> cf;

    @Autowired
    public void setCf(Flow<Credentials> cf){
        this.cf = cf;
    }

    @Override
    public boolean canHandle(String command) {
        return NAME.equals(command);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void run() throws Exception{

        Credentials creds = cf.run();

        BitbucketResponse response = bb.getUser(creds);

        if(response.statusCode == 200) {
            response.responseBody.getObject().remove("repositories");
            org.json.JSONObject body = response.responseBody.getObject();
            io.display(body.toString(4));
        } else {
            io.display("Problem calling the service. Response code: " + response.statusCode);
        }
    }
}
