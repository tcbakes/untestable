package com.tcbakes.untestable;

import java.io.IOException;

/**
 * An app that runs given a context described by a list of arguments
 */
public interface ArgDrivenApp {

    /**
     * Run the application
     * @param args Arguments that describe how the app should run
     * @throws Exception if there is a problem running the application
     */
    void run(String[] args) throws Exception;
}
