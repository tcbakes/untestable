package com.tcbakes.untestable;

import com.tcbakes.untestable.spring.ConsoleConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


/**
 * Even more testable!
 * <p/>
 * A bootstrapper for getting the app started.  Args passed to {@link #main(String[])} are passed
 * as-is to the underlying application.
 */
public class Untestable
{
    static Class config;

    /**
     * Returns a default application configuration, otherwise returns a (presumably) custom one
     * @return
     */
    static Class getConfig(){
        if(config == null)
            return ConsoleConfig.class;
        else
            return config;
    }

    /**
     * Boot-strap the application
     * @param args arguments to pass along to the application
     * @throws Exception if anything goes wrong
     */
    public static void main( String[] args ) throws Exception {

        AnnotationConfigApplicationContext context  = new AnnotationConfigApplicationContext(getConfig());
        ArgDrivenApp app = context.getBean(ArgDrivenApp.class);
        app.run(args);

    }
}
