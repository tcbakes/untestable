package com.tcbakes.untestable;

import com.tcbakes.untestable.command.Command;
import com.tcbakes.untestable.io.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 * A client that interacts with the BitBucket REST services.  The interaction with the services
 * is left to a collection of configured commands.  This class acts as a "router" matching a requested
 * command with an available configured command and delegating the operation as appropriate.
 */
public class BitbucketClientApp implements ArgDrivenApp {

    /**
     * A list of {@link Command}s that are handle-able by this application
     */
    private List<Command> commands;

    @Autowired
    public void setCommands(List<Command> commandList){
        this.commands = commandList;
    }

    /**
     * A {@link UserIO} instance for collecting info from and displaying info
     * to the user.
     */
    protected UserIO io;

    @Autowired
    public void setIo(UserIO io) {
        this.io = io;
    }


    @Override
    public void run(String[] args) throws Exception {

        if(args.length == 0){
            String message = "No command provided. Nothing to do.\n" +
                    "Supported commands are:\n";
            for(Command c : commands){
                message += "'" + c.getName() + "'\n";
            }
            io.display(message);
            return;
        }

        //Find the command that can handle the request
        boolean handled = false;
        for(Command c : commands){
            if(c.canHandle(args[0])) {
                handled = true;
                c.run();
            }
        }

        if(!handled) {
            //Unknown command
            io.display("Unrecognized command '" + args[0] + "'");
        }
    }
}
