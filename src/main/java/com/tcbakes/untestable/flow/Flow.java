package com.tcbakes.untestable.flow;

/**
 * A re-usable user interaction that displays/collects information
 * @param <O> The type encapsulating the data collected by the flow
 */
public interface Flow<O> {

    /**
     * Runs the flow, returns an object encapsulating the data collected
     * @return The data collected
     * @throws Exception some unrecoverable error has occured
     */
    O run() throws Exception;

}
