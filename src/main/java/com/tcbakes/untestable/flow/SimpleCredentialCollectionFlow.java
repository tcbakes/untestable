package com.tcbakes.untestable.flow;

import com.tcbakes.untestable.io.CredentialPersistence;
import com.tcbakes.untestable.io.Credentials;
import com.tcbakes.untestable.io.UserIO;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A re-usable flow for collecting credentials
 */
public class SimpleCredentialCollectionFlow implements Flow<Credentials> {

    /**
     * A {@link UserIO} instance for collecting info from and displaying info
     * to the user.
     */
    protected UserIO io;

    @Autowired
    public void setIo(UserIO io) {
        this.io = io;
    }

    /**
     * A {@link CredentialPersistence} instance for access to credentials
     */
    private CredentialPersistence cp;

    @Autowired
    public void setCp(CredentialPersistence cp){
        this.cp = cp;
    }


    @Override
    public Credentials run() throws Exception {
        //See if there are any stored credentials to use
        Credentials creds = cp.retrieve();

        //Collect them
        if (creds == null) {
            creds = new Credentials(
                    io.collect("Username: "),
                    io.collectSensitive("Password: "));

            //See if the user would like them persisted for use next time
            if (io.collect("Remember [y/n]: ").equalsIgnoreCase("y")) {
                cp.persist(creds);
            }
        }

        return creds;
    }
}
