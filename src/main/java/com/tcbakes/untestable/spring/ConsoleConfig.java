package com.tcbakes.untestable.spring;

import com.tcbakes.untestable.ArgDrivenApp;
import com.tcbakes.untestable.BitbucketClientApp;
import com.tcbakes.untestable.command.Command;
import com.tcbakes.untestable.command.GUserCommand;
import com.tcbakes.untestable.command.GUserReposCommand;
import com.tcbakes.untestable.flow.Flow;
import com.tcbakes.untestable.flow.SimpleCredentialCollectionFlow;
import com.tcbakes.untestable.io.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Spring configurations that sets up a BitbuketClientApp with:
 * <ol>
 *     <li>a collection of supported {@link Command}s</li>
 *     <li>an {@link ConsoleIO} instance for using the console for interfacing with the user</li>
 *     <li>an {@link RestBBClient} instance for access to the bitbucket remote REST services</li>
 *     <li>an {@link FileBasedCredentialPersistence} instance access to credentials persisted to the local
 *     filesystem</li>
 * </ol>
 */
@ComponentScan({"com.tcbakes.untestable"})
@Configuration
public class ConsoleConfig {

    @Autowired
    GUserCommand gUserCommand;

    @Autowired
    GUserReposCommand gUserReposCommand;


    @Bean
    public List<Command> getCommands(){
        ArrayList<Command> commands = new ArrayList<>();
        commands.add(gUserCommand);
        commands.add(gUserReposCommand);
        return commands;
    }

    @Bean
    public GUserCommand getgUserCommand(){
        return new GUserCommand();
    }

    @Bean
    public GUserReposCommand getgUserReposCommand(){
        return new GUserReposCommand();
    }

    @Bean
    public UserIO getIO(){
        return new ConsoleIO();
    }

    @Bean
    public ArgDrivenApp getApp(){
        return new BitbucketClientApp();
    }

    @Bean
    public BitbucketClient getClient(){
        return new RestBBClient();
    }

    @Bean
    public CredentialPersistence getCredPersistence(){
        return new FileBasedCredentialPersistence(Paths.get(System.getProperty("user.home"), ".untestable").toFile());
    }

    @Autowired
    UserIO userIO;

    @Autowired
    CredentialPersistence credPersist;

    @Bean
    public Flow<Credentials> getCredentialsFlow(){
        SimpleCredentialCollectionFlow flow = new SimpleCredentialCollectionFlow();
        flow.setCp(credPersist);
        flow.setIo(userIO);
        return flow;
    }
}
